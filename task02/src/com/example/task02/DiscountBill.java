package com.example.task02;

public class DiscountBill extends Bill {
    private double discountNum;

    public DiscountBill() {
        super();
    }

    public DiscountBill(double num) {
        super();
        this.discountNum = num;
    }

    public double getDiscount() {
        return discountNum*100;
    }
    public long getPrice() {
        long basePrice = super.getPrice();

        return (long) (basePrice - basePrice * discountNum);
    }

    public long getAbsoluteDiscount() {
        return super.getPrice() - this.getPrice();
    }
}
