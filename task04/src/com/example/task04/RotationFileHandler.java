package com.example.task04;

import lombok.NonNull;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class RotationFileHandler extends FileHandler implements MessageHandler {

    private ChronoUnit chronoUnit=ChronoUnit.DAYS;
    private long interval=1;

    public RotationFileHandler(){};

    RotationFileHandler(@NonNull ChronoUnit chronoUnit,long interval,@NonNull String path,@NonNull String name,@NonNull String type)
    {
        super(path, name, type);

        this.chronoUnit=chronoUnit;
        this.interval=interval;
    }

    RotationFileHandler(@NonNull ChronoUnit chronoUnit,long interval,@NonNull String name,@NonNull String type)
    {
        super(name, type);

        this.chronoUnit=chronoUnit;
        this.interval=interval;
    }


    @Override
    public void getOut(@NonNull String Message) {

        String fileName = String.format("%s:%s.%s",
                super.path+super.name,
                Instant.now().toString().replace(':', '.'),
                super.type);

        try(FileWriter writer = new FileWriter(fileName, true))
        {
            writer.write(Message);
        }
        catch(IOException ex){
            throw new RuntimeException(ex);
        }
    }
}
