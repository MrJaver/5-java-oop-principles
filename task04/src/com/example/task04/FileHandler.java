package com.example.task04;

import lombok.NonNull;

import java.io.FileWriter;
import java.io.IOException;

public class FileHandler implements MessageHandler{

    protected String path="";

    protected String name="logs";

    protected String type="txt";

    public FileHandler(){};

    public FileHandler(@NonNull String name,@NonNull String type)
    {

        this.name=name;
        this.type=type;
    }
    public FileHandler(@NonNull String path,@NonNull String name,@NonNull String type)
    {
        this.path=path;
        this.name=name;
        this.type=type;
    }


    @Override
    public void getOut(@NonNull String Message) {

        try(FileWriter writer = new FileWriter(path+name+"."+type, true))
        {
            writer.write(Message);
        }
        catch(IOException ex){

            throw new RuntimeException(ex);
        }

    }
}
