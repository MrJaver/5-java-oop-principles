package com.example.task04;

import lombok.NonNull;
public interface MessageHandler {

    void getOut(@NonNull String Message);
}
