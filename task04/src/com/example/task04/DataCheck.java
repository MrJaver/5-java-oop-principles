package com.example.task04;

public interface DataCheck {
   default <T> void isNull(T Data) {
       if (Data == null)
           throw new IllegalArgumentException("агрумент не может быть null");
   }

}
