package com.example.task03;

/**
 * Класс, в котором собраны методы для работы с {@link TimeUnit}
 */
public class TimeUnitUtils {

    private static void isNull(TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("агрумент не может быть null");
        }
    }

    public static Milliseconds toMillis(Seconds seconds) {
        isNull(seconds);
        return new Milliseconds(seconds.toMillis());
    }

    public static Milliseconds toMillis(Minutes minutes) {
        isNull(minutes);
        return new Milliseconds(minutes.toMillis());
    }

    public static Milliseconds toMillis(Hours hours) {
        isNull(hours);
        return new Milliseconds(hours.toMillis());
    }

    public static Seconds toSeconds(Milliseconds millis) {
        isNull(millis);
        return new Seconds(millis.toSeconds());
    }

    public static Seconds toSeconds(Minutes minutes) {
        isNull(minutes);
        return new Seconds(minutes.toSeconds());
    }

    public static Seconds toSeconds(Hours hours) {
        isNull(hours);
        return new Seconds(hours.toSeconds());
    }

    public static Minutes toMinutes(Milliseconds millis) {
        isNull(millis);
        return new Minutes(millis.toMinutes());
    }

    public static Minutes toMinutes(Seconds seconds) {
        isNull(seconds);
        return new Minutes(seconds.toMinutes());
    }

    public static Minutes toMinutes(Hours hours) {
        isNull(hours);
        return new Minutes(hours.toMinutes());
    }

    public static Hours toHours(Milliseconds millis) {
        isNull(millis);
        return new Hours(millis.toHours());
    }

    public static Hours toHours(Seconds seconds) {
        isNull(seconds);
        return new Hours(seconds.toHours());
    }

    public static Hours toHours(Minutes minutes) {
        isNull(minutes);
        return new Hours(minutes.toHours());
    }
}
