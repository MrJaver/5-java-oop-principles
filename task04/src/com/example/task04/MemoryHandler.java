package com.example.task04;

import lombok.NonNull;

import java.util.ArrayList;
public class MemoryHandler implements MessageHandler{

    private MessageHandler handler = new ConsoleHandler();

    private ArrayList<String> messages=new ArrayList<>();

    private static int counter=3;

    private void isThisClass(@NonNull MessageHandler handler)
    {
        if(this.getClass()==handler.getClass())
            throw new IllegalArgumentException("Аргументом класса MemoryHandler не может быть MemoryHandler");
    }

    public MemoryHandler(){};//нужен для создания объекта MemoryHandler с уже заданными параметрами (смотрите private)
    public MemoryHandler(@NonNull MessageHandler handler) {

        isThisClass(handler);

        this.handler=handler;
    }

    @Override
    public void getOut(@NonNull String Message) {
        messages.add(Message);

        if(messages.size()==counter)
        {
            for(String s:messages)
                handler.getOut(s);
            messages.clear();
        }
    }

    public MessageHandler getHandler() {
        return handler;
    }

    public void setHandler(@NonNull MessageHandler handler) {
        isThisClass(handler);

        this.handler = handler;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        MemoryHandler.counter = counter;
    }
}
