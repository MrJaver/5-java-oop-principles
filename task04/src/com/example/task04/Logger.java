package com.example.task04;

import lombok.NonNull;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
public class Logger{
    private enum Level {
        DEBUG,
        INFO,
        WARNING,
        ERROR
    }

    private static final String messageFormat = "[%s] %s %s %s - ";
    private static final String dateFormat = "yyyy.MM.dd";
    private static final String timeFormat = "HH:mm:ss";

    private static final Map<String,Logger> loggers = new HashMap<>();
    private String name;
    private Level level = Level.DEBUG;

    private MessageHandler handler = new ConsoleHandler();

    public Logger() {
    }

    public Logger(@NonNull String name) { //важно оставлять public
        this.name = name;
        loggers.put(name,this);
    }

    public void setLevel(@NonNull Level level) {
        this.level = level;
    }

    public Level getLevel() {

        return level;
    }

    public String getName() {
        return name;
    }

    public MessageHandler getHandler() {
        return handler;
    }

    public void setHandler(@NonNull MessageHandler handler) {
        this.handler = handler;
    }

    public static Logger getLogger(@NonNull String name) {
        loggers.putIfAbsent(name, new Logger(name));
        return loggers.get(name);
    }

    private String getFormatStringForLevel(@NonNull Level level) {

        return String.format(messageFormat, level.toString(),
                new SimpleDateFormat(dateFormat).format(LocalDateTime.now()),
                new SimpleDateFormat(timeFormat).format(LocalDateTime.now()),
                this.name);
    }

    protected String getLog(@NonNull Level level, @NonNull String message) {
        if (this.level.ordinal() <= level.ordinal()) {
            return getFormatStringForLevel(level) + message + "\n";
        }
        return "Not enough access";
    }

    protected String getLog(@NonNull Level level,@NonNull String format,@NonNull Object... values) {
        if (this.level.ordinal() <= level.ordinal()) {
            return String.format(format, values);
        }
        return "Not enough access";
    }

    public void debug(@NonNull String message) {
        handler.getOut(getLog(Level.DEBUG, message));
    }

    public void debug(@NonNull String format,@NonNull Object... values) {

        handler.getOut(getLog(Level.DEBUG, format, values));
    }

    public void info(@NonNull String message) {

        handler.getOut(getLog(Level.INFO, message));
    }

    public void info(@NonNull String format,@NonNull Object... values) {

        handler.getOut(getLog(Level.INFO, format, values));
    }

    public void warning(@NonNull String message) {

        handler.getOut(getLog(Level.WARNING, message));
    }

    public void warning(@NonNull String format,@NonNull Object... values) {

        handler.getOut(getLog(Level.WARNING, format, values));
    }

    public void error(@NonNull String message) {

        handler.getOut(getLog(Level.ERROR, message));
    }

    public void error(@NonNull String format,@NonNull Object... values) {

        handler.getOut(getLog(Level.ERROR, format, values));
    }

}
