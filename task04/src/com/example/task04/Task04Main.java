package com.example.task04;

import java.time.temporal.ChronoUnit;

public class Task04Main {
    public static void main(String[] args) {

        Logger logger =new Logger("logger");
        logger.debug("debug");
        Logger lg = new Logger("lg");
        lg.setHandler(new FileHandler("logs","txt"));
        lg.error("error");
        Logger lg2 = new Logger("lg2");
        lg2.setHandler(new MemoryHandler(new ConsoleHandler()));
        lg2.error("error");
        lg2.error("error");
        Logger lg3 =new Logger("lg3");
        lg3.setHandler(new RotationFileHandler(ChronoUnit.SECONDS,1,"logs2","txt"));


    }
}
